module.exports = {
	register:function(app, controllers){
		app.get('/', controllers.homepage);
		app.get('/sql', controllers.sql);
		app.get('/css', controllers.renderCss);
		app.get('/login', controllers.login);
		app.post('/login', controllers.authorize);
		/*app.get('/session', function(req, res){
			var cnt = (req.session.visitCount || 0) + 1;
			console.log(cnt);
			req.session.visitCount = cnt;
			res.send(200, "<html><body><p>"+cnt+"</p></body></html>");

		});*/
		app.get('*', controllers.send404);
	}
}