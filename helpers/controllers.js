var _ = require('underscore').noConflict();

var crypto = require('crypto');
var less = require('less');
var sql = require('mysql');
var ejs = require('ejs');
var fs = require('fs');
var express = require('express');


module.exports = {
	homepage:function(req, res){
		if(req.session.isUserLoggedIn){
			articles = [];
			res.redirect(301, "/login");
		}else{
		var articles = [{
			title:"Log In",
			content:"\
				Lorem ipsum dolor sit amet,\
				consectetur adipiscing elit. Aenean blandit, enim id posuere accumsan, tortor ligula tempus elit, ut laoreet felis elit at lacus. Nullasit amet,\
				consectetur adipiscing elit. Aenean blandit, enim id posuere accumsan, tortor ligula tempus elit, ut laoreet felis elit at lacus. Nullasit amet,\
				consectetur adipiscing elit. Aenean blandit, enim id posuere accumsan, tortor ligula tempus elit, ut laoreet felis elit at lacus. Nullasit amet,\
				consectetur adipiscing elit. Aenean blandit, enim id posuere accumsan, tortor ligula tempus elit, ut laoreet felis elit at lacus. Nullasit amet,\
				consectetur adipiscing elit. Aenean blandit, enim id posuere accumsan, tortor ligula tempus elit, ut laoreet felis elit at lacus. Nulla pharetra volutpat."
			}];
			res.render('body', { articles:articles });
		}

	}, login:function (req, res){
		res.render('login', {salt : crypto.createHash('sha256').update('The time to strike is ' + (new Date().getTime())).digest("hex")});

	}, authorize:function (req, res){
		res.send(req.body);

	}, renderCss:function (req, res){
		res.set('Content-Type', 'text/css').sendfile('./style.css');

	}, send404:function (req, res){
		res.status(404).send('File not Found');

	}, sql:function(req, res){
		var conn = sql.createConnection({
			host:'localhost',
			user:'root',
			//password:'discordistheebondragon',
			database:"chronicler"
		});

		conn.connect(function(err){
			if (err) console.log(err), res.send(500, 'Database connection error.');
			else {
				//conn.query("CREATE DATABASE IF NOT EXISTS `Chronicler`;", function(err, result){ res.send(200).json(result); });
				conn.query("SELECT * FROM `chronicler`.`users`", function(err, result){
					console.log(err, result);
					if(result.length)
						res.json(200, err, result);
					else if (!err)
						res.send(200, "Mysql returned empty result set");
					else
						res.json(500, err);
				});
			}
		});
	}
}