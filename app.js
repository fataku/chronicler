
var ejs = require('ejs');
var http = require('http');
var express = require("express");
var routs = require('./helpers/routing.js');
var controls = require('./helpers/controllers.js');

var app = express();


app.use('/partials', express.static(__dirname + '/partials'));
app.use('/assets', express.static(__dirname + '/assets'));
app.use('/views', express.static(__dirname + '/views'));

app.use(express.cookieParser('Discord is the ebon dragon'));
app.use(express.cookieSession());
app.use(express.bodyParser());

app.set('view engine', 'ejs');
app.engine('ejs', ejs.renderFile);
app.engine('html', ejs.renderFile);

routs.register(app, controls);

http.createServer(app).listen(9828, function(){
	if(process && process.send){
		process.send('online');
	}
});

if(process && process.on){
	process.on('message', function(msg){
		if(msg === 'shutdown'){
			process.exit(0);
		}
	})
}