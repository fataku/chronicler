#!/usr/bin/env python3
import sys, os, json, fileinput, re

class CRUD:
	read = ""
	create = ""
	update = ""
	delete = ""
	generate = ""

	createTemplate = """
	CREATE DEFINER = `root`@`localhost` PROCEDURE create_%(name)s (%(args)s
	)
	BEGIN
		%(extras)s
		INSERT INTO %(table)s (%(cols)s)
		VALUES (%(vars)s);
	END !!
	"""
	readTemplate = """
	CREATE DEFINER = `root`@`localhost` PROCEDURE get_%(name)s (%(args)s
	)
	BEGIN
		%(extras)s
		SELECT %(cols)s FROM `%(table)s` t
		WHERE
			%(vars)s;
	END!!
	"""
	updateTemplate = ""
	deleteTemplate = ""
	generateTemplate = ""

	schema = {}

	def generate_db(self):
		self.generate += "DROP DATABASE IF EXISTS %(db)s;\nCREATE DATABASE %(db)s;\nuse %(db)s;" % dict(db=self.schema['name'])

		postop = []
		tables = self.schema['tables']
		for tname, table in tables.items():
			self.generate += "\nCREATE TABLE IF NOT EXISTS %s (\n" % tname;

			if 'columns' in table :
				cols = [(str(i['name']), str(i['type'])) for i in table['columns']];
				self.generate += ",\n".join(["    `%s` %s" % (col[0], col[1]) for col in cols])

			if 'keys' in table :
				for key_type, key in table['keys'].items():
					if key :
						self.generate += {
							"primary":",\n    PRIMARY KEY (%s)",
							"unique":",\n    UNIQUE KEY (%s)",
						}.get(key_type, ",\n    PRIMARY KEY (%s)") % key
					else :
						self.generate += "\n    # got empty %s key, moving on" % key_type

			if 'references' in table :
				for ref_name, ref in table['references'] .items():
					if ref :
						op = "ALTER TABLE %s ADD FOREIGN KEY (%s) REFERENCES `%s`(%s)" % (tname, ref_name, ref['ref'].split('.')[0], ref['ref'].split('.')[1])
						#if ref['onUpdate'] : op += "\n    ON UPDATE %s" % ref['onUpdate']
						#if ref['onDelete'] : op += "\n    ON DELETE %s" % ref['onDelete']
						postop.append(op)

			self.generate += '\n)'

			self.generate += ' ENGINE=%s' % (table['engine'] if 'engine' in table and table['engine'] else "InnoDB")
			self.generate += ' DEFAULT CHARSET=%s' % (table['engine'] if 'charset' in table and table['charset'] else "utf8")
			self.generate += ' AUTO_INCREMENT=%s' % (table['engine'] if 'autoIncrement' in table and table['autoIncrement'] else "1")
			self.generate += ";\n"

		self.generate += "\n" + ";\n".join([str(i) for i in postop]) + ";"

	def generate_create(self):
		self.create = "DELIMITER !!\n"
		self.read = "DELIMITER !!\n"

		for tname, table in self.schema['tables'].items():
			if 'columns' in table :

				create = dict(name=tname, table=tname, db=self.schema['name'], extras=[], args=[], cols=[], vars=[])
				read   = dict(name=tname, table=tname, db=self.schema['name'], extras=[], args=[], cols=[], vars=[])
				update = dict(name=tname, table=tname, db=self.schema['name'], extras=[], args=[], cols=[], vars=[])
				delete = dict(name=tname, table=tname, db=self.schema['name'], extras=[], args=[], cols=[], vars=[])

				cols = table['columns'];
				for col in cols :
					cName = str(col['name'])
					cType = str(col['type']).split(" ")
					cVars = str(col['vars']) if 'vars' in col else None
					cFunc = str(col['function']) if 'function' in col else None

					create["args"].append("\n\t\tIN v_%s %s" % (cName, cType[0]))
					create["cols"].append(cName)
					create["vars"].append((cFunc%"v_%s" if cFunc else "v_%s")%cName)

					read["args"].append("\n\t\tIN v_%s %s" % (cName, cType[0]))
					read["cols"].append(cName)
					read["vars"].append(( ("t.%s " + ("=" if "VARCHAR" not in cType[0] else "LIKE") + (" v_%s" if not cFunc else " %s")) % (cName, cName if not cFunc else cFunc % ("v_"+cName))))

					if cFunc: # user-specified functions
						sql_vars = re.findall('v_\w+', cFunc)
						parse_vars = dict({i['name']:"t." + i['name'] for i in cols})
						for sv in sql_vars:
							vCreate = col['vars'][sv]['create']
							cRead = col['vars'][sv]['read']%parse_vars;

							create['extras'].append("set %s = %s;" % (sv, vCreate))
							read['vars'][-1] = re.sub(sv, cRead, read["vars"][-1])

					tread = dict(name=tname+"_by_"+col['name'], table=tname, db=self.schema['name'], extras=[], args=[], cols=[], vars=[]);
					tread["args"] = ", ".join(read["args"])
					tread["cols"] = ", ".join(read["cols"])
					tread["vars"] = " OR\n\t\t\t".join(read['vars'])
					tread["extras"] = "\n\t\t".join(read['extras'])
					if col['selectable'] : self.read +=  self.readTemplate % tread

				create["args"] = ", ".join(create["args"])
				create["cols"] = ", ".join(create["cols"])
				create["vars"] = ", ".join(create['vars'])
				create["extras"] = "\n\t\t".join(create['extras'])

				print(read['args'])

				read["args"] = ", ".join(read["args"])
				read["cols"] = ", ".join(read["cols"])
				read["vars"] = " OR\n\t\t\t".join(read['vars'])
				read["extras"] = "\n\t\t".join(read['extras'])

				#for i in

				self.create +=  self.createTemplate % create
				#self.read +=  self.readTemplate % read

		self.create += "\n DELIMITER ;"
		self.read += "\n DELIMITER ;"

	def generate_read(self):
		pass

		self.read = "DELIMITER ;\n"
		pass
	def generate_update(self):
		pass
	def generate_delete(self):
		pass

	def __init__(self, f):
		self.schema = json.loads(f)

		if 'name' not in self.schema or not self.schema['name'] :
			raise Exception("\n\n# " + "="*40 + "\n# No DB name provided. Halting execution.\n# " + "="*40 + "\n")
		else:
			self.generate_db();
			self.generate_read();
			self.generate_create();
			self.generate_update();
			self.generate_delete();

			print("\n\n# " + "="*80 + "\n#  TABLE GENERATION PROCEDURES" + "\n# " + "="*80 + "\n")
			print(self.generate)
			print("\n\n# " + "="*80 + "\n#  CREATE PROCEDURES" + "\n# " + "="*80 + "\n")
			print(self.create)
			print("\n\n# " + "="*80 + "\n#  READ PROCEDURES" + "\n# " + "="*80 + "\n")
			print(self.read)
		return None;

if __name__ == "__main__":
	content = ""
	for line in fileinput.input():
		content += line
	app = CRUD(content)
